O:Today we mainly learned about unit testing. We have written unit testing code for several business scenarios under the guidance of our teacher. We also learned about TDD, also known as "test driven development". In this programming philosophy, we first write tests, and then write business logic based on the tests, gradually improving the code. We have also deepened our understanding of this idea in practical practice.

R:I feel new.

I:Unit testing is not very familiar to me, and the concept of "TDD" is even more so. Today's study made me feel very novel and I also learned a lot of new knowledge.

D:Today's course taught me the writing and importance of unit testing, and I will pay more attention to the step of unit testing in the development process and apply it.