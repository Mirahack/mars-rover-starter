package com.afs.tdd;

public class MarsRover {

    private Location location;

    public Location getLocation() {
        return location;
    }

    public MarsRover(Location location) {
        this.location = location;
    }

    public void execCommand(Command command) {
        if (command == Command.Move) {
            move();
        }
        if (command == Command.TurnLeft) {
            turnLeft();
        }
        if (command == Command.TurnRight) {
            turnRight();
        }
        if (command == Command.Back) {
            back();
        }
    }

    private void back() {
        if (location.getDirection() == Direction.North) {
            this.location.setY(this.location.getY() - 1);
        }
        if (location.getDirection() == Direction.South) {
            this.location.setY(this.location.getY() + 1);
        }
        if (location.getDirection() == Direction.West) {
            this.location.setX(this.location.getX() + 1);
        }
        if (location.getDirection() == Direction.East) {
            this.location.setX(this.location.getX() - 1);
        }
    }

    private void turnRight() {
        if (location.getDirection() == Direction.North) {
            this.location.setDirection(Direction.East);
            return;
        }
        if (location.getDirection() == Direction.South) {
            this.location.setDirection(Direction.West);
            return;
        }
        if (location.getDirection() == Direction.West) {
            this.location.setDirection(Direction.North);
            return;
        }
        if (location.getDirection() == Direction.East) {
            this.location.setDirection(Direction.South);
        }
    }

    private void move() {
        if (location.getDirection() == Direction.North) {
            this.location.setY(this.location.getY() + 1);
        }
        if (location.getDirection() == Direction.South) {
            this.location.setY(this.location.getY() - 1);
        }
        if (location.getDirection() == Direction.West) {
            this.location.setX(this.location.getX() - 1);
        }
        if (location.getDirection() == Direction.East) {
            this.location.setX(this.location.getX() + 1);
        }
    }

    private void turnLeft() {
        if (location.getDirection() == Direction.North) {
            this.location.setDirection(Direction.West);
            return;
        }
        if (location.getDirection() == Direction.South) {
            this.location.setDirection(Direction.East);
            return;
        }
        if (location.getDirection() == Direction.West) {
            this.location.setDirection(Direction.South);
            return;
        }
        if (location.getDirection() == Direction.East) {
            this.location.setDirection(Direction.North);
        }
    }

    public void execBatchCommand(Command ...commands) {
        for (Command command : commands) {
            execCommand(command);
        }
    }
}
