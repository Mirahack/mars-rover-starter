package com.afs.tdd;

public class BackCommand implements AllCommand {
    private MarsRover marsRover;

    public BackCommand(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    @Override
    public void excute() {
        this.marsRover.execCommand(Command.Back);
    }
}
