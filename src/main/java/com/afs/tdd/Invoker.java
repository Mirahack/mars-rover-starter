package com.afs.tdd;


public class Invoker {
    private AllCommand command = null;

    private AllCommand[] commands = null;

    public void setCommand(AllCommand command) {
        this.command = command;
    }

    public void invoke() {
        command.excute();
    }

    public void setBatchCommand(AllCommand ...commands) {
        this.commands = commands;
    }

    public void invokeBatch() {
        for (AllCommand command : commands) {
            command.excute();
        }
    }
}
