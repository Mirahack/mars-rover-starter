package com.afs.tdd;

public class TurnLeftCommand implements AllCommand{

    private MarsRover marsRover;

    public TurnLeftCommand(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    @Override
    public void excute() {
        this.marsRover.execCommand(Command.TurnLeft);
    }
}
