package com.afs.tdd;

import java.util.Objects;

public class Location {

    private int x;

    private int y;

    private Direction direction;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Location(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Location anotherLocation = (Location) obj;
        return this.x == anotherLocation.x && this.y == anotherLocation.y && this.direction == anotherLocation.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.x, this.y, this.direction);
    }

}
