package com.afs.tdd;

public class MoveCommand implements AllCommand{

    private MarsRover marsRover;

    public MoveCommand(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    @Override
    public void excute() {
        this.marsRover.execCommand(Command.Move);
    }
}
