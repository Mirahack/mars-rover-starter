package com.afs.tdd;

public class TurnRightCommand implements AllCommand{

    private MarsRover marsRover;

    public TurnRightCommand(MarsRover marsRover) {
        this.marsRover = marsRover;
    }

    @Override
    public void excute() {
        this.marsRover.execCommand(Command.TurnRight);
    }
}
