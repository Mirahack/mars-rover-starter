package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MarsRoverTest {
    @Test
    void should_only_plus_y_when_move_given_location_with_north_direction() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.Move);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 1, Direction.North));
    }

    @Test
    void should_only_reduce_y_when_move_given_location_with_south_direction() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.Move);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, -1, Direction.South));
    }

    @Test
    void should_only_reduce_x_when_move_given_location_with_west_direction() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.Move);
        Assertions.assertEquals(marsRover.getLocation(), new Location(-1, 0, Direction.West));
    }

    @Test
    void should_only_plus_x_when_move_given_location_with_east_direction() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.Move);
        Assertions.assertEquals(marsRover.getLocation(), new Location(1, 0, Direction.East));
    }

    @Test
    void should_only_change_direction_to_west_when_turn_left_given_location_with_north_direction() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnLeft);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.West));
    }

    @Test
    void should_only_change_direction_to_east_when_turn_left_given_location_with_south_direction() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnLeft);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.East));
    }

    @Test
    void should_only_change_direction_to_south_when_turn_left_given_location_with_west_direction() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnLeft);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.South));
    }

    @Test
    void should_only_change_direction_to_north_when_turn_left_given_location_with_east_direction() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnLeft);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.North));
    }

    @Test
    void should_only_change_direction_to_east_when_turn_right_given_location_with_north_direction() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnRight);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.East));
    }

    @Test
    void should_only_change_direction_to_west_when_turn_right_given_location_with_south_direction() {
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnRight);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.West));
    }

    @Test
    void should_only_change_direction_to_north_when_turn_right_given_location_with_west_direction() {
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnRight);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.North));
    }

    @Test
    void should_only_change_direction_to_south_when_turn_right_given_location_with_east_direction() {
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.TurnRight);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, 0, Direction.South));
    }

    @Test
    void should_match_the_right_location_when_exec_specific_batch_command_given_location() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execBatchCommand(Command.TurnRight, Command.Move, Command.TurnLeft, Command.Move, Command.Move, Command.TurnLeft);
        Assertions.assertEquals(marsRover.getLocation(), new Location(1, 2, Direction.West));
    }


    @Test
    void should_match_the_right_location_when_exec_specific_command_given_location_using_command_mode() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        Invoker invoker = new Invoker();
        MoveCommand moveCommand = new MoveCommand(marsRover);
        TurnLeftCommand turnLeftCommand = new TurnLeftCommand(marsRover);
        TurnRightCommand turnRightCommand = new TurnRightCommand(marsRover);
        invoker.setCommand(turnLeftCommand);
        invoker.invoke();
        invoker.setCommand(moveCommand);
        invoker.invoke();
        invoker.setCommand(turnRightCommand);
        invoker.invoke();
        Assertions.assertEquals(marsRover.getLocation(), new Location(-1, 0, Direction.North));
    }

    @Test
    void should_match_the_right_location_when_exec_specific_batch_command_given_location_using_command_mode() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        Invoker invoker = new Invoker();
        invoker.setBatchCommand(new TurnLeftCommand(marsRover), new MoveCommand(marsRover), new TurnRightCommand(marsRover));
        invoker.invokeBatch();
        Assertions.assertEquals(marsRover.getLocation(), new Location(-1, 0, Direction.North));
    }

    @Test
    void should_only_reduce_y_when_back_given_location_with_north_direction() {
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.execCommand(Command.Back);
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, -1, Direction.North));
        Invoker invoker = new Invoker();
        invoker.setCommand(new BackCommand(marsRover));
        invoker.invoke();
        Assertions.assertEquals(marsRover.getLocation(), new Location(0, -2, Direction.North));
    }
}
